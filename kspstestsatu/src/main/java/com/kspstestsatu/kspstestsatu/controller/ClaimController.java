package com.kspstestsatu.kspstestsatu.controller;

import com.kspstestsatu.kspstestsatu.dto.request.Item;
import com.kspstestsatu.kspstestsatu.dto.request.RegisterRequest;
import com.kspstestsatu.kspstestsatu.dto.request.VerifyRequest;
import com.kspstestsatu.kspstestsatu.dto.response.*;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api
@RequestMapping("/claim")
@RestController
public class ClaimController {
    //belum sempat test semua di insomnia, dan belum sempat screen soot hasil test, tapi runing tidak eror

    @PostMapping("/register")
    public RegisterResponse.RegisterResponseBuilder claimRegister(@RequestBody RegisterRequest registerRequest){
      var registerRequests = RegisterRequest.builder()
              .cardNumber("654002")
              .hospitalName("Hermina Depok")
              .admissionDate("2021-01-01")
              .item(List.of(Item.builder()
                              .name("Dokter Umum")
                              .value(75000.0)
                      .build(),
                      Item.builder()
                              .name("Obat -obatan")
                              .value(20000.0)
                              .build()
                      )).build();

      if (registerRequest == null){
          responseFailed();

      }
      var registerResponse = RegisterResponse.builder()
              .code("200")
              .message("Sukses")
              .data(List.of(Data.builder()
                              .claimNumber("C0001")
                      .build()));
    return registerResponse;
    }

    public ResponseFailed responseFailed(){
        var responseFailed = ResponseFailed.builder()
                .code("400")
                .message("Card Number wajib diisi")
                .build();
        return responseFailed;

    }


    @PostMapping("/verify")
    public VerifyResponse claimVerify(@RequestBody VerifyRequest verifyRequest){
        var verifyRequests =VerifyRequest.builder()
                .claimNumber("C0001")
                .build();
        if (verifyRequests == null){
            responseFailed();
        }
        var verifyResponse = VerifyResponse.builder()
                .code("200")
                .message("Verifikasi Klaim C0001 berhasil")
                .build();

        return verifyResponse;
    }

    public GetClaimResponse.GetClaimResponseBuilder getAllDataClaim(){
        var getClaimAll= GetClaimResponse.builder()
                .code("200")
                .message("Sukses")
                .data(List.of(Data.builder()
                                .claimNumber("C0001")
                                .memberName("AKBAR")
                                .statusClaim("SUBMIT")
                                .item(List.of(Item.builder()
                                                        .name("Dokter Umum")
                                                        .value(75000.0)
                                                .build(),
                                        Item.builder()
                                                .name("Obaat-obatan")
                                                .value(20000.0)
                                                .build()
                                        ))
                        .build()

                ));
return getClaimAll;
    }

    public GetClaimFailedResponse allDataResponseFailed(){
        var getClaimFailed = GetClaimFailedResponse.builder()
                .code("400")
                .message("Data tidak ditemukan")
                .build();

        return getClaimFailed;
    }

    @GetMapping("/get-all-data")
    public void getAllData(){
        String checkConnection = "Jaringan Bagus";
        if (checkConnection !=null){
            getAllDataClaim();
        }
        else {
            allDataResponseFailed();
        }
    }

}
