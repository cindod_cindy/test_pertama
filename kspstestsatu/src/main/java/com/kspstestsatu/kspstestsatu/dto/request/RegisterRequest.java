package com.kspstestsatu.kspstestsatu.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegisterRequest {

    private String cardNumber;
    private String hospitalName;
    private String admissionDate;
    private List<Item> item;
}
