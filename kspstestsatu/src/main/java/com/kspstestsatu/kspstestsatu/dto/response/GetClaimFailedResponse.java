package com.kspstestsatu.kspstestsatu.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@lombok.Data
public class GetClaimFailedResponse {
    private String code;
    private String message;
}
