package com.kspstestsatu.kspstestsatu.dto.response;

import com.kspstestsatu.kspstestsatu.dto.request.Item;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@lombok.Data
public class Data {

    private String claimNumber;
    private String memberName;
    private String statusClaim;
    private List<Item> item;
}
