package com.kspstestsatu.kspstestsatu.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterResponse {
    private String code;
    private String message;
    private List<com.kspstestsatu.kspstestsatu.dto.response.Data> data;
}
