package com.kspstestsatu.kspstestsatu.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@lombok.Data
public class GetClaimResponse {
    private String code;
    private String message;
    private List<Data> data;
}
